import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
# TODO: add necessary imports here


# TODO: change values here
EPOCHS = 1
BATCH_SIZE = 1

# Preparation to read data from a directory
BASE_DIRECTORY = '/travail/m2irt/TP_galaxyzoo/'
catalog = pd.read_csv(BASE_DIRECTORY+'train.csv', sep=',')
DIRECTORY = BASE_DIRECTORY+'train/'
catalog_test = pd.read_csv(BASE_DIRECTORY+'test.csv', sep=',')
DIRECTORY_TEST = BASE_DIRECTORY+'test/'

def normalize(fn):
    return str(fn)+".jpg"

catalog['GalaxyID'] = catalog['GalaxyID'].apply(normalize)
catalog_test['GalaxyID'] = catalog_test['GalaxyID'].apply(normalize)

datagen = ImageDataGenerator(rescale=1./255.,validation_split=0.25)

train_generator = datagen.flow_from_dataframe(
    dataframe=catalog,
    directory=DIRECTORY,
    x_col="GalaxyID",
    y_col=['Class1.1', 'Class1.2', 'Class1.3'],
    subset="training",
    batch_size=BATCH_SIZE,
    seed=42,
    shuffle=True,
    class_mode="raw",
    target_size=(256,256))


valid_generator = datagen.flow_from_dataframe(
    dataframe=catalog,
    directory=DIRECTORY,
    x_col="GalaxyID",
    y_col=['Class1.1', 'Class1.2', 'Class1.3'],
    subset="validation",
    batch_size=BATCH_SIZE,
    seed=42,
    shuffle=True,
    class_mode="raw",
    target_size=(256,256))

datagen_test = ImageDataGenerator(rescale=1./255.)
test_generator = datagen_test.flow_from_dataframe(
    dataframe=catalog_test,
    directory=DIRECTORY_TEST,
    x_col="GalaxyID",
    y_col=None,
    batch_size=BATCH_SIZE,
    class_mode=None,
    target_size=(256,256))


model = Sequential()
# TODO: define your model here!

model.compile(optimizer='adam', loss="categorical_crossentropy",metrics=["accuracy"])
print(model.summary())

# train the model
model.fit(train_generator, epochs=EPOCHS)

# check on the validation set
model.evaluate(valid_generator)

# predict values for the test set
pred = model.predict(test_generator)

# save results
catalog_test.loc[:, 'Class1.1'] = pred[:, 0]
catalog_test.loc[:, 'Class1.2'] = pred[:, 1]
catalog_test.loc[:, 'Class1.3'] = pred[:, 2]
catalog_test.to_csv('test_result.csv', index=False)

# TODO: send me your result!
