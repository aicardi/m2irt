#!/bin/sh
### 
###  You can change here the job-name, the time and the email obviously. 
###  Don't touch the rest or it won't work!
###
#SBATCH --job-name=TP_galaxyzoo
#SBATCH --time=00:15:00  
#SBATCH --mail-user=YOUR_EMAIL@obspm.fr --mail-type=ALL
#SBATCH --clusters=gpu
#SBATCH --partition=def
#SBATCH --qos=gpu_def_long
#SBATCH --account=m2irt_hpchpda
#SBATCH --nodes=1
#SBATCH --gpus-per-node=1

cd ~/m2irt/IA   # or whatever directory you have put your python script

module load cuda cudnn python
export https_proxy='proxy.obspm.fr:3128'
. /travail/m2irt/pyenv/bin/activate

python TP_galaxyzoo.py

exit 0
