import matplotlib.pyplot as plt
import numpy as np

# function to calculate the error
def mse(truth, pred):
    return np.mean((truth-pred)**2)

# to ensure reproducibility
RANDOM_GEN = np.random.default_rng(1)

# number of points
SIZE = 50
A = np.linspace(0., 10., num=SIZE)
B = RANDOM_GEN.normal(0, .5, SIZE)
B += (A-2)**2/6.4
# will be used to plot a nicer regression curve
A_highres = np.linspace(0., 10., num=SIZE*10)

# shuffle our dataset
TRAIN = int(SIZE*.8)
p = RANDOM_GEN.permutation(SIZE)

# train dataset will consist of 80% of the data points
X_train = A[p][:TRAIN]
Y_train = B[p][:TRAIN]

# train dataset will consist of the other 20% of the data points
X_test = A[p][TRAIN:]
Y_test = B[p][TRAIN:]

# plot the results (one plot per degree)
for degree in [1, 2, 3, 5, 20]:
    lin_reg_coeffs = np.polyfit(X_train, Y_train, degree)
    lin_reg = np.poly1d(lin_reg_coeffs)
    print('Degree =', degree)

    plt.scatter(X_train, Y_train, marker='x')
    plt.scatter(X_test, Y_test, marker='x', color='red')
    plt.plot(A_highres, lin_reg(A_highres), color='green')
    print('Training MSE:', mse(Y_train, lin_reg(X_train)))
    print('Testing MSE:', mse(Y_test, lin_reg(X_test)))

    plt.show()
    print()


