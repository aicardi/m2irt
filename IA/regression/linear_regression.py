import matplotlib.pyplot as plt
import numpy as np

# function to calculate the error
def mse(truth, pred):
    return np.mean((truth-pred)**2)

# to ensure reproducibility
RANDOM_GEN = np.random.default_rng(1)

# number of points
SIZE = 100


# build our dataset : Y = X + gaussian noise
A = np.linspace(0., 10., num=SIZE)
B = RANDOM_GEN.normal(0, .1, SIZE)
B += A


# shuffle our dataset
p = np.random.permutation(SIZE)
X_train = A[p]
Y_train = B[p]


# perform linear regression
lin_reg_coeffs = np.polyfit(X_train, Y_train, 1)
lin_reg = np.poly1d(lin_reg_coeffs)
print(lin_reg_coeffs)

# plot the result
scat = plt.scatter(X_train, Y_train, marker='x')
scat.set_label('Data')
plot, = plt.plot(A, lin_reg(A), color='green')
plot.set_label(f'y = {lin_reg_coeffs[0]:.2f} x + {lin_reg_coeffs[1]:.2f} ')
print('Training MSE:', mse(Y_train, lin_reg(X_train)))
plt.legend()
plt.show()
print()


