from tensorflow.keras.datasets import fashion_mnist as mnist
import matplotlib.pyplot as plt

# Load the dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.astype('float32')

plt.figure(figsize=(20,4))
for i in range(10):
    ax = plt.subplot(2, 5, i+1)
    ax.set_xticks([], [])
    ax.set_yticks([], [])
    plt.imshow(x_train[i])
    plt.gray()
    plt.xlabel(y_train[i], fontsize=20)
plt.show()
