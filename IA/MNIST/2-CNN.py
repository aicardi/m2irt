from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.datasets import mnist

# Load the datasets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Prepare the datasets
x_train = x_train.reshape(60000, 28, 28, 1).astype('float32')/255
x_test = x_test.reshape(10000, 28, 28, 1).astype('float32')/255

# Convert labels to one hot encoding
y_train = to_categorical(y_train, 10)
y_test = to_categorical(y_test, 10)

# Some variables
BATCH_SIZE = 128
EPOCHS = 12
PADDING = 'valid'

# Build the model
network = Sequential()
network.add(Conv2D(32, input_shape=(28,28,1), kernel_size=(3, 3), padding=PADDING, activation='relu'))
network.add(MaxPooling2D(pool_size=(2, 2)))
network.add(Conv2D(64, kernel_size=(3, 3), padding=PADDING, activation='relu'))
network.add(MaxPooling2D(pool_size=(2, 2)))
network.add(Flatten())
network.add(Dense(10, activation='softmax'))

# Show the model
print(network.summary())

# Train the model
network.compile(loss='categorical_crossentropy', optimizer='adam', metrics='accuracy')
network.fit(x_train, y_train, batch_size=BATCH_SIZE, epochs=EPOCHS)

# Validate the model on the test dataset
score = network.evaluate(x_test, y_test, verbose=0)
print('Test score: ', score[0])
print('Test accuracy: ', score[1])
