from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten, BatchNormalization
from tensorflow.keras.datasets import mnist

# Load the dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.reshape(60000, 28, 28, 1).astype('float32')/255
x_test = x_test.reshape(10000, 28, 28, 1).astype('float32')/255

y_train = to_categorical(y_train, 10)
y_test = to_categorical(y_test, 10)

BATCH_SIZE = 128
EPOCHS = 12
#PADDING = 'same'
PADDING = 'valid'

network = Sequential()
#network.add(BatchNormalization(input_shape=(28,28,1)))
network.add(Conv2D(32, input_shape=(28,28,1), kernel_size=(3, 3), padding=PADDING, activation='relu'))
#network.add(Conv2D(32, kernel_size=(3, 3), padding=PADDING, activation='relu'))
network.add(BatchNormalization())
network.add(MaxPooling2D(pool_size=(2, 2)))
network.add(Conv2D(64, kernel_size=(3, 3), padding=PADDING, activation='relu'))
network.add(BatchNormalization())
network.add(MaxPooling2D(pool_size=(2, 2)))
#network.add(Conv2D(128, kernel_size=(3, 3), padding=PADDING, activation='relu'))
#network.add(MaxPooling2D(pool_size=(2, 2)))
network.add(Flatten())
network.add(Dropout(.2))
network.add(Dense(10, activation='softmax'))

print(network.summary())

network.compile(loss='categorical_crossentropy', optimizer='adam', metrics='accuracy')
network.fit(x_train, y_train, batch_size=BATCH_SIZE, epochs=EPOCHS)

score = network.evaluate(x_test, y_test, verbose=0)
print('Test score: ', score[0])
print('Test accuracy: ', score[1])
