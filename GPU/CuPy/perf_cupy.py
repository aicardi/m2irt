import numpy as np
import cupy as cp
import timeit
import sys

SIZES = [16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192]
RUNS = 20

numpy_result = {}
cupy_result = {}
cupy_transfer_result = {}

# preload functions
A = cp.random.rand(10, 10)
A = A + A
A = cp.dot(A, A)

for size in SIZES:
    print(f'{size} ')
    A = np.random.rand(size,size)
    B = np.random.rand(size,size)
    
    C = A.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        C = C + B
    end = timeit.default_timer()
    numpy_result[f'Add_{size}'] = (end-start)/RUNS

    D = A.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        D = np.dot(D, B)
    end = timeit.default_timer()
    numpy_result[f'Dot_{size}'] = (end-start)/RUNS
    
    A_gpu = cp.asarray(A)
    B_gpu = cp.asarray(B)
    
    C_gpu = A_gpu.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        C_gpu = C_gpu + B_gpu
    end = timeit.default_timer()
    cupy_result[f'Add_{size}'] = (end-start)/RUNS
    C_cpu = cp.asnumpy(C_gpu)
    if np.linalg.norm(C-C_cpu)> 1e-6*np.linalg.norm(C):
       print('Bug with +')
       print(C)
       print(C_cpu)
       sys.exit(1)

    C = A.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        C_gpu = cp.asarray(C)
        C_gpu = C_gpu + B_gpu
        C = cp.asnumpy(C_gpu)
    end = timeit.default_timer()
    cupy_transfer_result[f'Add_{size}'] = (end-start)/RUNS

    D_gpu = A_gpu.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        D_gpu = cp.dot(D_gpu, B_gpu)
    end = timeit.default_timer()
    cupy_result[f'Dot_{size}'] = (end-start)/RUNS
    D_cpu = cp.asnumpy(D_gpu)
    if np.linalg.norm(D-D_cpu)> 1e-6*np.linalg.norm(D):
       print('Bug with dot')
       print(D)
       print(D_cpu)
       print(D_cpu-D)
       sys.exit(1)
    
    D = A.copy()
    start = timeit.default_timer()
    for _ in range(RUNS):
        D_gpu = cp.asarray(D)
        D_gpu = cp.dot(D_gpu, B_gpu)
        D = cp.asnumpy(D_gpu)
    end = timeit.default_timer()
    cupy_transfer_result[f'Dot_{size}'] = (end-start)/RUNS
print()
print('%(test)-10s|%(numpy)-10s|%(cupy)-10s|%(cupy_t)-10s|%(speedup)-10s|%(speedup_t)-10s|'%{'test':'TEST',
                                                              'numpy':'NumPy',
                                                              'cupy':'CuPy',
                                                              'cupy_t':'CuPy mem.',
                                                              'speedup':'Seedup',
                                                              'speedup_t':'Seedup mem'
                                                              })
for key in numpy_result:
    print('%(test)-10s|%(numpy)10.4f|%(cupy)10.4f|%(cupy_t)10.4f|%(speedup)10.4f|%(speedup_t)10.4f|'%{'test': key,
                                                  'numpy': numpy_result[key],
                                                  'cupy': cupy_result[key],
                                                  'cupy_t': cupy_transfer_result[key],
                                                  'speedup': numpy_result[key]/cupy_result[key],
                                                  'speedup_t': numpy_result[key]/cupy_transfer_result[key]
                                                  })
