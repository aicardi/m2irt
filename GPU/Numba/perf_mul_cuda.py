import math
import numpy as np
from numba import cuda, float32
import timeit

# fix the number of threads by blocks to 32, because 32*32=1024=max of threads by blocks
TPB = 32

@cuda.jit
def fast_matmul(A, B, C):
    # Define an array in the shared memory
    # The size and type of the arrays must be known at compile time
    sA = cuda.shared.array(shape=(TPB, TPB), dtype=float32)
    sB = cuda.shared.array(shape=(TPB, TPB), dtype=float32)

    x, y = cuda.grid(2)

    tx = cuda.threadIdx.x
    ty = cuda.threadIdx.y
    bpg = cuda.gridDim.x    # blocks per grid

    if x >= C.shape[0] and y >= C.shape[1]:
        # Quit if (x, y) is outside of valid C boundary
        return

    # Each thread computes one element in the result matrix.
    # The dot product is chunked into dot products of TPB-long vectors.
    tmp = 0.
    for i in range(bpg):
        # Preload data into shared memory
        sA[tx, ty] = A[x, ty + i * TPB]
        sB[tx, ty] = B[tx + i * TPB, y]

        # Wait until all threads finish preloading
        cuda.syncthreads()

        # Computes partial product on the shared memory
        for j in range(TPB):
            tmp += sA[tx, j] * sB[j, ty]

        # Wait until all threads finish computing
        cuda.syncthreads()

    C[x, y] = tmp

RUNS = 10
N = 1<<10

# initialize the data on the host
x = np.random.rand(N,N)
y = np.random.rand(N,N)
z = np.zeros((N,N))

# prefetch the data to the GPU
d_x = cuda.to_device(x)
d_y = cuda.to_device(y)
d_z = cuda.to_device(z)

# define grid and block sizes
threadsperblock = (TPB, TPB)
blockspergrid_x = math.ceil(z.shape[0] / threadsperblock[0])
blockspergrid_y = math.ceil(z.shape[1] / threadsperblock[1])
blockspergrid = (blockspergrid_x, blockspergrid_y)

# Precompile
fast_matmul[blockspergrid, threadsperblock](d_x, d_y, d_z)

# time RUNS multiplications on the GPU
start = timeit.default_timer()
for _ in range(RUNS):
    fast_matmul[blockspergrid, threadsperblock](d_x, d_y, d_z)
end = timeit.default_timer()
time_fast_cuda = (end-start)/RUNS
print('Time per run', time_fast_cuda)

# fetch data from GPU
z = d_z.copy_to_host()

# check for errors
maxError = np.max(np.abs(z-np.dot(x, y)))
print('Max error:', maxError)
#print(z, np.dot(x,y))

# time RUNS multiplications with numpy for comparison
start = timeit.default_timer()
for _ in range(RUNS):
    z = np.dot(x, y)
end = timeit.default_timer()
time_numpy = (end-start)/RUNS
print('Time per run', time_numpy)

print('Speed-up', time_numpy/time_fast_cuda)


