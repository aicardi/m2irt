# M2IRT

## GPU

To access to mesopsl :
* first ssh to styx.obspm.fr if you are not on site
* then ssh mesopsl.obspm.fr

To launch an interactive session on a server with a GPU, type:

    srun --job-name "Test CUDA" --nodes=1 --ntasks-per-node=8 --time 0:15:00 --account=m2irt_hpchpda --qos=gpu_def_long --partition=def --clusters=gpu --gres=gpu:nvidia:0 --pty bash

Your prompt will change to the new machine, which should be node0155, node0256, node0257 or node0258.

Type:

    module load cuda python gcc/7.3.0


Now, you should be able to use nvcc and nvprof

## IA

Examples will be run on mesopsl. Using an interactive session would have been great but it doesn't work sadly. So you will have to run batches.

Debugging code via batches can be tedious. I suggest to check your python syntax on the frontal node with these commands:

    module load cuda cudnn python
    export https_proxy='proxy.obspm.fr:3128'
    . /travail/m2irt/pyenv/bin/activate
    python your_code.py

If the training begins, cancel the execution with Ctrl-C and use a batch to run on a GPU.

## Running batches on MesoPSL

If you prefer to lauch batches instead of using an interactive session, you can modify what is needed in the file [batch.sh](IA/batch.sh) in the [IA](IA) directory, then lauch your batch with the command

    sbatch batch.sh

The result will be in the a file called slurm-JOBID.out, JOBID is the number that is assigned to your job:

    [aicardi@mesopsl IA]$ sbatch batch.sh 
    Submitted batch job 1982 on cluster gpu


More explanations are available in French on the [MesoPSL website](https://wwwmesopsl-new.obspm.fr/slurm/).


NB: If you use the skeleton I gave you, the slurm-....out will contain lots of awful characters. To have a cleaner view, replace the model.fit line by

    model.fit(train_generator, epochs=EPOCHS, verbose=2)

With this option verbose=2, your script will print only one line per epoch and not the animation, it's much better for a batch script.
